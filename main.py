from fastapi import FastAPI, Depends, HTTPException, Request
from sqlalchemy import create_engine, text, Table, MetaData, inspect
from sqlalchemy.orm import sessionmaker
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware

# Database configuration
DATABASE_URL = "postgresql://user:password@localhost/DBname"
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
metadata = MetaData()

# Create the FastAPI app
app = FastAPI()

# Enable CORS for all origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Dependency to get the database session
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# Route to read data
@app.get("/{table_name}/{column_name}/{value}")
def read_table(table_name: str, column_name: str = None, value = None, db=Depends(get_db)):
    try:
        with db.connection() as conn:
            # Security check to prevent SQL injection
            if not is_valid_table_name(table_name):
                raise HTTPException(status_code=400, detail="Invalid table name.")
            
            # Query to fetch column names from the specified table
            column_query = text(f"SELECT column_name FROM information_schema.columns WHERE table_name = :table_name")
            column_result = conn.execute(column_query, {"table_name": table_name}).fetchall()
            column_names = [col[0] for col in column_result]

            # Construct the dynamic query safely
            if column_name != None and value != None:
                data_query = text(f"SELECT * FROM {table_name} WHERE {column_name} = :value")
                data_result = conn.execute(data_query, {"value": value}).fetchall()

            # Convert result to a list of dictionaries
            rows = [dict(zip(column_names, row)) for row in data_result]
            return rows
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal server error: {str(e)}")

@app.get("/{table_name}/")
def read_table(table_name: str, db=Depends(get_db)):
    try:
        with db.connection() as conn:
            # Security check to prevent SQL injection
            if not is_valid_table_name(table_name):
                raise HTTPException(status_code=400, detail="Invalid table name.")
            
            # Query to fetch column names from the specified table
            column_query = text(f"SELECT column_name FROM information_schema.columns WHERE table_name = :table_name")
            column_result = conn.execute(column_query, {"table_name": table_name}).fetchall()
            column_names = [col[0] for col in column_result]

            # Dynamic query to fetch all rows and columns from the specified table
            data_query = text(f"SELECT * FROM {table_name}")
            data_result = conn.execute(data_query).fetchall()

            # Convert result to a list of dictionaries
            rows = [dict(zip(column_names, row)) for row in data_result]
            return rows
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"Internal server error: {str(e)}")

@app.post("/{table_name}/")
async def insert_data(table_name: str, request: Request, db=Depends(get_db)):
    # Parse JSON body of the request
    data = await request.json()

    with db.connection() as conn:
        # Security check to prevent SQL injection
        if not is_valid_table_name(table_name):
            raise HTTPException(status_code=400, detail="Invalid table name.")
        
        # Reflect the table from the database
        table_to_insert = Table(table_name, metadata, autoload_with=engine)
        
        # Insert the data into the table
        query = table_to_insert.insert().values(**data)
        conn.execute(query)
        conn.commit()
        
        return {"status": "success", "data": data}



# Route to insert data into a specified table
@app.put("/{table_name}/{column_name}/{value}/")
async def update_data(table_name: str, column_name: str, value, request: Request, db=Depends(get_db)):
    # Parse JSON body of the request
    update_data = await request.json()

    with db.connection() as conn:
        # Security check to prevent SQL injection
        if not is_valid_table_name(table_name):
            raise HTTPException(status_code=400, detail="Invalid table name.")
        
        # Reflect the table from the database
        table_to_update = Table(table_name, metadata, autoload_with=engine)
        
        # Get the primary key column name
        primary_key_column = get_primary_key_column_name(table_name, db)
        
        # Update the data in the table
        query = table_to_update.update().where(table_to_update.c[primary_key_column] == value).values(**update_data)
        
        conn.execute(query)
        conn.commit()
        
        return {"status": "success", "data": update_data}

@app.delete("/{table_name}/{column_name}/{value}/")
async def delete_data(table_name: str, column_name: str, value, db=Depends(get_db)):
    with db.connection() as conn:
        # Security check to prevent SQL injection
        if not is_valid_table_name(table_name):
            raise HTTPException(status_code=400, detail="Invalid table name.")
        
        # Reflect the table from the database
        table_to_delete = Table(table_name, metadata, autoload_with=engine)
        
        # Delete the data from the table
        query = table_to_delete.delete().where(table_to_delete.c[column_name] == value)
        
        conn.execute(query)
        conn.commit()
        
        return {"status": "success", "message": "Data deleted successfully"}

def get_primary_key_column_name(table_name: str, db):
    """
    Returns the primary key column name(s) for a given table.
    """
    db_url = "postgresql://user:password@localhost/DBname"  # Update with your database URL
    engine = create_engine(db_url)
    
    # Create an inspector to inspect the table
    inspector = inspect(engine)
    
    # Get the primary key column name(s)
    primary_key_columns = inspector.get_pk_constraint(table_name)['constrained_columns']
    
    return primary_key_columns

def is_valid_table_name(table_name):
    # Implement a check to ensure the table_name is valid
    # For example, check against a list of allowed table names
    return table_name in ["table_name-1", "table_name-2"]
